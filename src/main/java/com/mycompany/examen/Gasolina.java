/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.examen;

/**
 *
 * @author sebas
 */
public class Gasolina {
    private int codigoVenta;
    private int cantidad;
    private int tipoGasolina;
    private float presio;

    public Gasolina() {
        this.codigoVenta=0;
        this.cantidad=0;
        this.tipoGasolina=0;
        this.presio=0.0f;
        
    }

    public Gasolina(int codigoVenta, int cantidad, int tipoGasolina, float presio) {
        this.codigoVenta = codigoVenta;
        this.cantidad = cantidad;
        this.tipoGasolina = tipoGasolina;
        this.presio = presio;
    }
    
    public Gasolina(Gasolina otro){
        this.codigoVenta = otro.codigoVenta;
        this.cantidad = otro.cantidad;
        this.tipoGasolina = otro.tipoGasolina;
        this.presio = otro.presio;
    }

    public int getCodigoVenta() {
        return codigoVenta;
    }

    public void setCodigoVenta(int codigoVenta) {
        this.codigoVenta = codigoVenta;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipoGasolina() {
        return tipoGasolina;
    }

    public void setTipoGasolina(int tipoGasolina) {
        this.tipoGasolina = tipoGasolina;
    }

    public float getPresio() {
        return presio;
    }

    public void setPresio(float presio) {
        this.presio = presio;
    }
    public float calcularCostoVenta(){
        float costoVenta= 0.0f;
        float precioGalon= 0.0f;
        
        //precio por galon de gasolina
        switch(this.tipoGasolina){
            case 1:
                precioGalon = 18.50f;
                break;
            case 2:
                precioGalon = 17.50f;
            default:
                System.out.println("ese tipo de gasolina no existe");
                break;
        }
        costoVenta = this.cantidad * precioGalon;
        return costoVenta;
        
    }
    
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = this.presio * .16f;
        return impuesto;

    }
    
    public float calcularTotal(){
        float total = 0.0f;
        total = this.calcularCostoVenta() + this.calcularImpuesto();
        return total;
    }
    
    public void imprimirGasolina(){
    System.out.println("Código de venta: " + this.codigoVenta);
    System.out.println("Cantidad: " + this.cantidad + " galones");
    System.out.println("Tipo de gasolina: " + this.tipoGasolina);
    System.out.println("Precio por galón: " + this.presio + " Bs.");
    System.out.println("Costo de venta: " + this.calcularCostoVenta() + " Bs.");
    System.out.println("Impuesto: " + this.calcularImpuesto() + " Bs.");
    System.out.println("Total: " + this.calcularTotal() + " Bs.");
    }
    
    
   
}
