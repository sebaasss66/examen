/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.mycompany.examen;

import javax.swing.JOptionPane;

/**
 *
 * @author sebas
 */
public class jintTicketGasolina extends javax.swing.JFrame {

    /**
     * Creates new form jintTicketGasolina
     */
    public jintTicketGasolina() {
        initComponents();
        setSize(800,700);
        
        this.setLocationRelativeTo(null);
        this.deshabilitar();
    }
    
    public void deshabilitar(){
        this.txtCodigoVenta.setEnabled(false);
        this.txtCantidad.setEnabled(false);
        this.cmbTipoGasolina.setEnabled(false);
        this.txtCodigoVenta.setEnabled(false);
        this.txtImpuesto.setEnabled(false);
        this.txtTotalPagar.setEnabled(false);
        this.txtCostoVenta.setEnabled(false);
        this.txtPresio.setEnabled(false);
        this.btmGuardar.setEnabled(false);
        this.btmCalcular.setEnabled(false);
        
    }
    public void habilitar(){
        this.txtCodigoVenta.setEnabled(!false);
        this.txtCantidad.setEnabled(!false);
        this.cmbTipoGasolina.setEnabled(!false);
        this.txtCodigoVenta.setEnabled(!false);
        this.btmGuardar.setEnabled(true);
        this.btmNuevo.setEnabled(true);
        this.btmLimpiar.setEnabled(true);
    }
    
    public void limpiar(){
        //limpiar textField
        
        this.txtCodigoVenta.setText("");
        this.txtCantidad.setText("");
        this.txtCodigoVenta.setText("");
        this.txtImpuesto.setText("");
        this.txtTotalPagar.setText("");
        this.txtPresio.setText("");
        this.txtCostoVenta.setText("");
        
        
        
        //poner en la primera posicion al combobox
        this.cmbTipoGasolina.setSelectedIndex(0);
        
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cmbTipoGasolina = new javax.swing.JComboBox<>();
        txtCodigoVenta = new javax.swing.JTextField();
        txtCantidad = new javax.swing.JTextField();
        btmNuevo = new javax.swing.JButton();
        btmGuardar = new javax.swing.JButton();
        btmCalcular = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtCostoVenta = new javax.swing.JTextField();
        txtImpuesto = new javax.swing.JTextField();
        txtTotalPagar = new javax.swing.JTextField();
        btmLimpiar = new javax.swing.JButton();
        btmCancelar = new javax.swing.JButton();
        btmCerrar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        txtPresio = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        jLabel1.setText("Codigo de Venta:");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(20, 40, 100, 16);

        jLabel2.setText("Cantidad:");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(20, 80, 60, 16);

        cmbTipoGasolina.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tipo de Galosina", "Premium", "Regular" }));
        getContentPane().add(cmbTipoGasolina);
        cmbTipoGasolina.setBounds(20, 120, 140, 22);
        getContentPane().add(txtCodigoVenta);
        txtCodigoVenta.setBounds(120, 40, 140, 22);
        getContentPane().add(txtCantidad);
        txtCantidad.setBounds(80, 80, 150, 22);

        btmNuevo.setText("Nuevo");
        btmNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btmNuevo);
        btmNuevo.setBounds(590, 20, 72, 23);

        btmGuardar.setText("Guardar");
        btmGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btmGuardar);
        btmGuardar.setBounds(590, 60, 72, 23);

        btmCalcular.setText("Calcular");
        btmCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmCalcularActionPerformed(evt);
            }
        });
        getContentPane().add(btmCalcular);
        btmCalcular.setBounds(590, 100, 73, 23);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(null);

        jLabel3.setText("Costo de Venta:");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(19, 25, 90, 16);

        jLabel4.setText("Impuesto:");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(20, 60, 60, 16);

        jLabel5.setText("Total a Pagar:");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(20, 100, 80, 16);
        jPanel1.add(txtCostoVenta);
        txtCostoVenta.setBounds(110, 20, 110, 22);
        jPanel1.add(txtImpuesto);
        txtImpuesto.setBounds(80, 60, 140, 22);
        jPanel1.add(txtTotalPagar);
        txtTotalPagar.setBounds(100, 100, 120, 22);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(220, 170, 470, 220);

        btmLimpiar.setText("Limpiar");
        btmLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btmLimpiar);
        btmLimpiar.setBounds(210, 430, 72, 23);

        btmCancelar.setText("Cancelar");
        btmCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btmCancelar);
        btmCancelar.setBounds(320, 430, 80, 23);

        btmCerrar.setText("Cerrar");
        btmCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btmCerrar);
        btmCerrar.setBounds(430, 430, 72, 23);

        jLabel6.setText("Presio:");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(20, 160, 50, 16);
        getContentPane().add(txtPresio);
        txtPresio.setBounds(70, 160, 110, 22);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btmNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmNuevoActionPerformed
        // TODO add your handling code here:
        gas = new Gasolina();
        this.habilitar();

    }//GEN-LAST:event_btmNuevoActionPerformed

    private void btmGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmGuardarActionPerformed
        // TODO add your handling code here:
        boolean exito = false;
        if(this.txtCodigoVenta.getText().isEmpty())exito = true;
        if(this.txtCantidad.getText().isEmpty())exito = true;

        if(exito){
            JOptionPane.showMessageDialog(this,"falto capturar informacion", "advertencia", JOptionPane.WARNING_MESSAGE);
        }
        else{
            gas.setCodigoVenta(Integer.parseInt(this.txtCodigoVenta.getText()));
            gas.setCantidad(Integer.parseInt(this.txtCantidad.getText()));
            
            JOptionPane.showMessageDialog(this, "se guardo con exito la informacion");
            this.btmCalcular.setEnabled(true);
        }

    }//GEN-LAST:event_btmGuardarActionPerformed

    private void btmCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmCalcularActionPerformed
        // TODO add your handling code here:
         String tipoGasolina = (String) cmbTipoGasolina.getSelectedItem();
    
    // Declara las variables para almacenar el costo y el impuesto
    double costo, impuesto, total;
    
    // Utiliza un condicional para determinar el precio según el tipo de gasolina seleccionado
    if (tipoGasolina.equalsIgnoreCase("Premium")) {
        costo = 18.50;
    } else if (tipoGasolina.equalsIgnoreCase("Regular")) {
        costo = 17.50;
    } else {
        // Si no se seleccionó ninguna opción válida, muestra un mensaje de error
        JOptionPane.showMessageDialog(this, "Por favor, seleccione un tipo de gasolina válido.", "Error", JOptionPane.ERROR_MESSAGE);
        return;
    }
    
    // Calcula el impuesto y el total
    impuesto = costo * 0.16;
    total = costo + impuesto;
    
    // Muestra los resultados en los campos de texto
    txtCostoVenta.setText(String.format("%.2f", costo));
    txtImpuesto.setText(String.format("%.2f", impuesto));
    txtTotalPagar.setText(String.format("%.2f", total));
    txtPresio.setText(String.valueOf(costo)); 

    }//GEN-LAST:event_btmCalcularActionPerformed

    private void btmLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmLimpiarActionPerformed
        // TODO add your handling code here:
        this.txtCodigoVenta.setText("");
        this.txtCantidad.setText("");
        this.txtCodigoVenta.setText("");
        this.txtImpuesto.setText("");
        this.txtTotalPagar.setText("");
        this.txtPresio.setText("");
        this.txtCostoVenta.setText("");

        //poner en la primera posicion al combobox
        this.cmbTipoGasolina.setSelectedIndex(0);
    }//GEN-LAST:event_btmLimpiarActionPerformed

    private void btmCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmCancelarActionPerformed
        // TODO add your handling code here:

        this.limpiar();
        this.deshabilitar();
    }//GEN-LAST:event_btmCancelarActionPerformed

    private void btmCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmCerrarActionPerformed
        // TODO add your handling code here:
        int opcion =0;
        opcion = JOptionPane.showConfirmDialog(this,"deseas salir",
            "Gasolina", JOptionPane.YES_NO_OPTION);

        if(opcion == JOptionPane.YES_OPTION){
            this.dispose();
        }

    }//GEN-LAST:event_btmCerrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jintTicketGasolina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jintTicketGasolina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jintTicketGasolina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jintTicketGasolina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new jintTicketGasolina().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btmCalcular;
    private javax.swing.JButton btmCancelar;
    private javax.swing.JButton btmCerrar;
    private javax.swing.JButton btmGuardar;
    private javax.swing.JButton btmLimpiar;
    private javax.swing.JButton btmNuevo;
    private javax.swing.JComboBox<String> cmbTipoGasolina;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtCodigoVenta;
    private javax.swing.JTextField txtCostoVenta;
    private javax.swing.JTextField txtImpuesto;
    private javax.swing.JTextField txtPresio;
    private javax.swing.JTextField txtTotalPagar;
    // End of variables declaration//GEN-END:variables
private Gasolina gas = new Gasolina();

}
